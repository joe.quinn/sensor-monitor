#!/usr/bin/env python
# encoding: utf-8
# pylint: disable=no-member
import schedule
import time
import logging
import importlib

from prometheus_client import start_http_server, Gauge
from environs import Env

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)s: %(message)s')
log = logging.getLogger(__name__)

env = Env()
env.read_env()  # read .env file if it exists
PORT = env.int('PORT', 8000)
READ_MINS = env.int('READ_MINS', 1)
LOCATION = env('LOCATION', 'unknown')

# dynamically load source - default to stub implementation
SOURCE_MODULE = env('SOURCE_MODULE', 'stub')
source = importlib.import_module(f'sources.{SOURCE_MODULE}')

# set up prometheus gauges
temp = Gauge('temp_c', 'Temperature in degrees C', ['location'])
humidity = Gauge('humidity_pc', 'Humidity percentage', ['location'])


def read_source():
    """ Read temperature and humidity from source (api, sensor, or stub) """
    try:
        temp_c, humidity_pc = source.read()
        log.debug(
            f'Temp/Humidity reading -> {temp_c}C {humidity_pc}% humidity')
        humidity.labels(LOCATION).set(humidity_pc)
        temp.labels(LOCATION).set(temp_c)
    except RuntimeError:
        log.error("Read error, skipping reading.")


if __name__ == '__main__':
    log.info(
        f'location is [{LOCATION}], source read mins [{READ_MINS}], listening on port [{PORT}]')
    # Start up the server to expose the metrics.
    read_source()  # do an upfront read to populate initial gauge values
    schedule.every(READ_MINS).minutes.do(read_source)
    start_http_server(PORT)
    # Schedule source reads.
    while True:
        schedule.run_pending()
        time.sleep(10)
