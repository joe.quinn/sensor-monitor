import logging
import random
import requests
from environs import Env
from typing import Tuple

log = logging.getLogger(__name__)

log.warn('*************** Temperature / Humidity from OpenWeatherMap ****************')

env = Env()
env.read_env()  # read .env file if it exists

API_KEY = env('API_KEY')
CITY_ID = env('CITY_ID')


def read() -> Tuple[str, str]:
    """ Reads the temperature in C, and humidity %"""
    r = requests.get(
        f'https://api.openweathermap.org/data/2.5/weather?id={CITY_ID}&appid={API_KEY}&units=metric')
    log.debug(
        f'openweathermap api temp reading: {r.status_code} \n {r.json()}')
    if r.status_code == 200:
        temp_c = r.json()['main']['temp']
        humidity_pc = r.json()['main']['humidity']
        log.debug(f'temp_c from api is {temp_c}')
        return temp_c, humidity_pc
    else:
        raise RuntimeError(
            f'Failed to call openweathermap api. Status code={r.status_code}')
