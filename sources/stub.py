import logging
import random
from typing import Tuple

log = logging.getLogger(__name__)

log.warn(
    '************** Using stubbed random values [TESTING-ONLY] **************')


def read() -> Tuple[str, str]:
    """ Reads the temperature in C and humidity %"""
    log.warn('sensor unavailable, generating dummy temperature/humidity reading')
    return random.randrange(1, 100), random.randrange(1, 100)
