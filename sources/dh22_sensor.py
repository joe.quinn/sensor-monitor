import adafruit_dht
import board
from typing import Tuple

dhtSensor = adafruit_dht.DHT22(board.D4)


def read() -> Tuple[str, str]:
    """ Reads the temperature in C, and humidity %"""
    return dhtSensor.temperature, dhtSensor.humidity
